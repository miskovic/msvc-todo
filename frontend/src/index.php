<html>
<title>Microservice Test</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.6/semantic.min.css">
<body>

  <div class="ui main text container">
    <h1 class="ui header">Backend Greeting</h1>
    <p>
    <pre><?php print file_get_contents(getenv('BACKEND_URL'))?>
    </pre>
    </p>
  </div>

  <div class="ui main text container">
    <div class="ui accordion" style="font-size: 75%;">

      <div class="title">
        <i class="dropdown icon"></i>
        $_SERVER
      </div>
      <div class="content">
        <pre><?php print_r($_SERVER); ?></pre>
      </div>

    </div>
  </div>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdn.jsdelivr.net/semantic-ui/2.2.6/semantic.min.js"></script>
  <script>
    $('.ui.accordion')
      .accordion()
    ;
  </script>
</body>
<html>
