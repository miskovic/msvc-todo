#!/usr/bin/env perl

use Mojolicious::Lite;

get '/' => sub {
    my $c  = shift;
    $c->render(json => {
        greeting => "Hello From Backend System",
        hostname => $ENV{HOSTNAME}
    });
  };

app->start;
