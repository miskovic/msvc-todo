
systems({
  frontend: {
    depends: [
      'backend',
    ],
    image: { docker: "php:5.6.27-apache" },
    // workdir: "/var/www/html",
    // shell: "/bin/bash",
    scalable: { default: 2 },
    wait: 50000,
    http: {
      domains: [ "#{system.name}.#{azk.default_domain}" ],
    },
    ports: {
      http: '80/tcp'
    },
    mounts: {
      '/var/www/html': path("./frontend/src"),
    },
  },

  backend: {
    provision: [
      // install all perl dependencies
      'carton install --deployment',
    ],
    depends: [],
    image: { dockerfile: "backend/Dockerfile" },
    scalable: { default: 2 },
    wait: 50000,
    workdir: "/backend",
    http: {
      domains: [ "#{system.name}.#{azk.default_domain}" ],
    },
    ports: {
      http: '3000/tcp'
    },
    command: "/backend/local/bin/morbo /backend/backend.pl",
    envs: {
      PERL5LIB: 'local/lib/perl5',
    },
    mounts: {
      '/backend': path("./backend"),
    },
  },


});
